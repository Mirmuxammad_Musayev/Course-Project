// Main.java
package org.example;

import org.example.Controller.ControllerImpl;
import org.example.Administrator.Administrator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        // Создаем администратора с заданными учетными данными
        Administrator administrator = new Administrator("admin", "adminpassword");

        // Создаем контроллер, передавая администратора
        ControllerImpl controller = new ControllerImpl(administrator);

        // Передаем scanner и reader в main метод
        try (Scanner scanner = new Scanner(System.in);
             BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/clothesandfootwearData.csv"))) {
            controller.main(scanner, reader);
        } catch (IOException e) {
            System.err.println("An error occurred while running the application: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
