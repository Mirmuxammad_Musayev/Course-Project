package org.example.DAO;

import org.example.Entity.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductDao {

    private final List<Product> products;
    private String filePath = "src/main/resources/clothesandfootwearData.csv";

    public ProductDao() {
        this.products = new ArrayList<>();
        this.filePath = filePath;
    }

    public List<Product> getAllProducts() throws IOException {
        List<Product> products = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] productDetails = line.split(",");
                products.add(new Product(Integer.parseInt(productDetails[0]), productDetails[1], productDetails[2], Double.parseDouble(productDetails[3]), Integer.parseInt(productDetails[4]), productDetails[5], productDetails[6]));
            }

        }
            return products;

    }

    public Product getProductById(int id) throws IOException {
        List<Product> productList= new ArrayList<>();
        for (Product product : getAllProducts()) {
            if (product.getId().equals(id)) {
                productList.add(product);
            }
        }
        if (productList.isEmpty()){
            System.err.println("Product not founded please refresh and get see id list!");
        }
        else {
            for (Product product : productList) {
                System.out.println(product);
            }
        }
        return new Product();
    }
    public Product getProductByName(String name) throws IOException {
        List<Product> productList= new ArrayList<>() ;
        for(Product product : getAllProducts()) {
            if (product.getName().equalsIgnoreCase(name)) {
                productList.add(product);
            }
        }
        if(productList.isEmpty()){
            System.err.println("Product not founded please refresh and see name by list!");
            }
               else{
                   for(Product product : productList){
                       System.out.println(product);
                   }
                }
               return new Product();
    }

    public Product getProductByType(String type) throws IOException {
        List<Product> productList= new ArrayList<>() ;
        for(Product product : getAllProducts()) {
            if (product.getType().equalsIgnoreCase(type)) {
                productList.add(product);
            }
        }
        if(productList.isEmpty()){
            System.err.println("Product not founded please refresh and see type by list!");
        }
        else{
            for(Product product : productList){
                System.out.println(product);
            }
        }
        return new Product();
    }
    public Product getProductByPrice(Double price) throws IOException {
        List<Product> productList= new ArrayList<>() ;
        for(Product product : getAllProducts()) {
            if (Objects.equals(product.getPrice(), price)) {
                productList.add(product);
            }
        }
        if(productList.isEmpty()){
            System.err.println("Product not founded please refresh and see price by list!");
        }
        else{
            for(Product product : productList){
                System.out.println(product);
            }
        }
        return new Product();
    }
    public Product getProductByQuantity(Integer quantity) throws IOException {
        List<Product> productList= new ArrayList<>() ;
        for(Product product : getAllProducts()) {
            if (product.getQuantity()==quantity) {
                productList.add(product);
            }
        }
        if(productList.isEmpty()){
            System.err.println("Product not founded please refresh and see quantity by list!");
        }
        else{
            for(Product product : productList){
                System.out.println(product);
            }
        }
        return new Product();
    }
    public Product getProductByBrand(String brand) throws IOException {
        List<Product> productList= new ArrayList<>() ;
        for(Product product : getAllProducts()) {
            if (product.getBrand().equalsIgnoreCase(brand)) {
                productList.add(product);
            }
        }
        if(productList.isEmpty()){
            System.err.println("Product not founded please refresh and see brand by list!");
        }
        else{
            for(Product product : productList){
                System.out.println(product);
            }
        }
        return new Product();
    }
    public Product getProductBySeason(String season) throws IOException {
        List<Product> productList= new ArrayList<>() ;
        for(Product product : getAllProducts()) {
            if (product.getSeason().equalsIgnoreCase(season)) {
                productList.add(product);
            }
        }
        if(productList.isEmpty()){
            System.err.println("Product not founded please refresh and see season by list!");
        }
        else{
            for(Product product : productList){
                System.out.println(product);
            }
        }
        return new Product();
    }

}
