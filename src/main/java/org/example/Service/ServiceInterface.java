package org.example.Service;

import org.example.Entity.Product;

import java.io.IOException;
import java.util.List;

public interface ServiceInterface {
    List<Product> getAll() throws IOException;
    Product getId(int id) throws IOException;
    Product getName(String name) throws IOException;
    Product getType(String type) throws IOException;
    Product getPrice(double price) throws IOException;
    Product getQuantity(int quantity) throws IOException;
    Product getBrand(String brand) throws IOException;
    Product getSeason(String season) throws IOException;




}
