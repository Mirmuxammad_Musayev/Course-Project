package org.example.Service;

import org.example.DAO.ProductDao;
import org.example.Entity.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Service implements ServiceInterface {
    private String filePath = "src/main/resources/clothesandfootwearData.csv";
    public ProductDao productDao;

    public Service(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<Product> getAll() throws IOException {
        return productDao.getAllProducts();
    }

    @Override
    public Product getId(int id) throws IOException {
        return productDao.getProductById(id);
    }

    @Override
    public  Product getName(String name) throws IOException {
        return productDao.getProductByName(name);
    }

    @Override
    public Product getType(String type) throws IOException {
        return productDao.getProductByType(type);
    }

    @Override
    public Product getPrice(double price) throws IOException {
        return productDao.getProductByPrice(price);
    }

    @Override
    public Product getQuantity(int quantity) throws IOException {
        return productDao.getProductByQuantity(quantity);
    }

    @Override
    public Product getBrand(String brand) throws IOException {
        return productDao.getProductByBrand(brand);
    }

    @Override
    public Product getSeason(String season) throws IOException {
        return productDao.getProductBySeason(season);
    }
}
