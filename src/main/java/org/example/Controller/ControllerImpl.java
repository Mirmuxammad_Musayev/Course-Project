// ControllerImpl.java
package org.example.Controller;

import org.example.Administrator.Administrator;
import org.example.DAO.ProductDao;
import org.example.Entity.Product;
import org.example.Service.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements ControllerInterface {
    private Administrator administrator;
    private List<Product> products;
    private ProductDao productDao = new ProductDao();
    private Service service = new Service(productDao);
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ControllerImpl(Administrator administrator) {
        this.administrator = administrator;
    }

    @Override
    public void main(Scanner scanner, BufferedReader reader) throws IOException {
        System.out.println("Welcome to the Clothes and Footwear Warehouse Application in Console");
        System.out.println("1. Administrator Login");
        System.out.println("2. User Login");
        System.out.println("3. Exit");

        System.out.print("Enter choice: ");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                adminLogin(scanner);
                break;

            case 2:
                userMenu(scanner);
                break;

            case 3:
                System.out.println("Exiting application...\n Thanks for choose us, We wish the best wishes)");
                return;

            default:
                System.err.println("Invalid choice. Exiting application.");
        }
    }

    @Override
    public void main() throws IOException {

    }

    @Override
    public void getAll(Scanner scanner) throws IOException {

    }

    @Override
    public void getId(Scanner scanner) throws IOException {

    }

    @Override
    public void getName(Scanner scanner) throws IOException {

    }

    @Override
    public void getType(Scanner scanner) throws IOException {

    }

    @Override
    public void getPrice(Scanner scanner) throws IOException {

    }

    @Override
    public void getQuantity(Scanner scanner) throws IOException {

    }

    @Override
    public void getBrand(Scanner scanner) throws IOException {

    }

    @Override
    public void getSeason(Scanner scanner) throws IOException {

    }

    private void adminLogin(Scanner scanner) throws IOException {
        System.out.println("Administrator Login");
        System.out.print("Enter administrator username: ");
        String enteredUsername = scanner.next();
        System.out.print("Enter administrator password: ");
        String enteredPassword = scanner.next();

        if (administrator.authenticate(enteredUsername, enteredPassword)) {
            adminMenu(scanner);
        } else {
            System.out.println("Authentication failed. Exiting application.");
        }
    }

    private void adminMenu(Scanner scanner) throws IOException {
        System.out.println("Administrator Menu");
        System.out.println("1. List all products");
        System.out.println("2. Get a product by ID");
        System.out.println("3. Get a product by name");
        System.out.println("4. Get a product by type");
        System.out.println("5. Get a product by price");
        System.out.println("6. Get a product by quantity");
        System.out.println("7. Get a product by brand");
        System.out.println("8. Get a product by season");
        System.out.println("9. Add new product");
        System.out.println("10. Remove product");
        System.out.println("11. Add new category");
        System.out.println("12. Exit app");

        while (true) {
            try {
                System.out.print("Enter command: ");
                int command = scanner.nextInt();

                switch (command) {
                    case 1:
                        getAll(scanner);
                        break;

                    case 2:
                        getId(scanner);
                        break;

                    case 3:
                        getName(scanner);
                        break;

                    case 4:
                        getType(scanner);
                        break;

                    case 5:
                        getPrice(scanner);
                        break;

                    case 6:
                        getQuantity(scanner);
                        break;

                    case 7:
                        getBrand(scanner);
                        break;

                    case 8:
                        getSeason(scanner);
                        break;

                    case 9:
                        addNewProduct(scanner);
                        break;

                    case 10:
                        removeProduct(scanner);
                        break;

                    case 11:
                        addNewCategory(scanner);
                        break;

                    case 12:
                        System.out.println("Exiting application...\n Thanks for choose us, We wish the best wishes)");
                        return;

                    default:
                        System.err.println("Invalid command. Please rewrite your command!");
                }
            } catch (InputMismatchException e) {
                System.err.println("Invalid command. Please enter numbers!!");
                scanner.nextLine();
            }
        }
    }

    private void userMenu(Scanner scanner) throws IOException {
        System.out.println("User Menu");
        System.out.println("1. List all products");
        System.out.println("2. Get a product by ID");
        System.out.println("3. Get a product by name");
        System.out.println("4. Get a product by type");
        System.out.println("5. Get a product by price");
        System.out.println("6. Get a product by quantity");
        System.out.println("7. Get a product by brand");
        System.out.println("8. Get a product by season");
        System.out.println("9. Exit app");

        while (true) {
            try {
                System.out.print("Enter command: ");
                int command = scanner.nextInt();

                switch (command) {
                    case 1:
                        getAll(scanner);
                        break;

                    case 2:
                        getId(scanner);
                        break;

                    case 3:
                        getName(scanner);
                        break;

                    case 4:
                        getType(scanner);
                        break;

                    case 5:
                        getPrice(scanner);
                        break;

                    case 6:
                        getQuantity(scanner);
                        break;

                    case 7:
                        getBrand(scanner);
                        break;

                    case 8:
                        getSeason(scanner);
                        break;

                    case 9:
                        System.out.println("Exiting application...\n Thanks for choose us, We wish the best wishes)");
                        return;

                    default:
                        System.err.println("Invalid command. Please rewrite your command!");
                }
            } catch (InputMismatchException e) {
                System.err.println("Invalid command. Please enter numbers!!");
                scanner.nextLine();
            }
        }
    }

    private void addNewProduct(Scanner scanner) throws IOException {
        // Implement logic to add a new product
    }

    private void removeProduct(Scanner scanner) throws IOException {
        // Implement logic to remove a product
    }

    private void addNewCategory(Scanner scanner) throws IOException {
        // Implement logic to add a new category
    }
}
