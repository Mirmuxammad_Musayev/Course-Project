package org.example.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

public interface ControllerInterface {
    void main(Scanner scanner, BufferedReader reader) throws IOException;

    void main() throws IOException;
    void getAll(Scanner scanner) throws IOException;
    void getId(Scanner scanner) throws IOException;
    void getName(Scanner scanner) throws IOException;
    void getType(Scanner scanner) throws IOException;
    void getPrice(Scanner scanner) throws IOException;
    void getQuantity(Scanner scanner) throws IOException;
    void getBrand(Scanner scanner) throws IOException;
    void getSeason(Scanner scanner) throws IOException;

}
