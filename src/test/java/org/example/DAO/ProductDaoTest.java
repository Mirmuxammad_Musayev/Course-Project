package org.example.DAO;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import org.example.DAO.ProductDao;
import org.example.Entity.Product;

import java.io.IOException;
import java.util.List;

public class ProductDaoTest {

    @Test
    public void testGetAllProducts() throws IOException {
        ProductDao productDao = new ProductDao();
        List<Product> products = productDao.getAllProducts();

        Assertions.assertEquals(10, products.size());

        Product product1 = products.get(0);
        Assertions.assertEquals(1, product1.getId());
        Assertions.assertEquals("Jacket", product1.getName());
        Assertions.assertEquals("Clothes", product1.getType());
        Assertions.assertEquals(126.0, product1.getPrice());
        Assertions.assertEquals(4, product1.getQuantity());
        Assertions.assertEquals("Armani", product1.getBrand());
        Assertions.assertEquals("winter", product1.getSeason());

        Product product2 = products.get(1);
        Assertions.assertEquals(2, product2.getId());
        Assertions.assertEquals("Hoodie", product2.getName());
        Assertions.assertEquals("Clothes", product2.getType());
        Assertions.assertEquals(187.0, product2.getPrice());
        Assertions.assertEquals(13, product2.getQuantity());
        Assertions.assertEquals("H&H", product2.getBrand());
        Assertions.assertEquals("spring", product2.getSeason());
    }

    @Test
    public void testGetProductById() throws IOException {
        ProductDao productDao = new ProductDao();
        Product product = productDao.getProductById(1);

        Assertions.assertEquals(1, product.getId());
        Assertions.assertEquals("Jacket", product.getName());
        Assertions.assertEquals("Clothes", product.getType());
        Assertions.assertEquals(126.0, product.getPrice());
        Assertions.assertEquals(4, product.getQuantity());
        Assertions.assertEquals("Armani", product.getBrand());
        Assertions.assertEquals("winter", product.getSeason());
    }

    @Test
    public void testGetProductByName() throws IOException {
        ProductDao productDao = new ProductDao();
        Product product = productDao.getProductByName("Jacket");

        Assertions.assertEquals(1, product.getId());
        Assertions.assertEquals("Jacket", product.getName());
        Assertions.assertEquals("Clothes", product.getType());
        Assertions.assertEquals(126.0, product.getPrice());
        Assertions.assertEquals(4, product.getQuantity());
        Assertions.assertEquals("Armani", product.getBrand());
        Assertions.assertEquals("winter", product.getSeason());
    }

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void getAllProducts() {
    }

    @org.junit.jupiter.api.Test
    void getProductById() {
    }

    @org.junit.jupiter.api.Test
    void getProductByName() {
    }

    @org.junit.jupiter.api.Test
    void getProductByType() {
    }

    @org.junit.jupiter.api.Test
    void getProductByPrice() {
    }

    @org.junit.jupiter.api.Test
    void getProductByQuantity() {
    }

    @org.junit.jupiter.api.Test
    void getProductByBrand() {
    }

    @org.junit.jupiter.api.Test
    void getProductBySeason() {
    }
}